# README #

Please Create a local instance in SQL Server called bluegrass and run the following code:

CREATE DATABASE Practical

USE Practical

CREATE TABLE [dbo].[PageInATable] (
    [PageId]   INT			 PRIMARY KEY IDENTITY (1, 1) NOT NULL,
    [PageName] NVARCHAR (50) UNIQUE NOT NULL,
    [ParentId] INT           NULL,
	FOREIGN KEY ([ParentId]) REFERENCES [PageInATable]([PageId])
);

INSERT INTO
	PageInATable (PageName, ParentId)
VALUES
	('Home', NULL),
	('Page1',1),
	('Page2',2),
	('Page3',2),
	('Page4',2),
	('Page5',3),
	('Page6',4),
	('Page7',2),
	('Page8',2),
	('Page9',7),
	('Page10',8),
	('Page11',9),
	('Page12',10),
	('Page13',12),
	('Page14',10),
	('Page15',14)