﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BluegrassDigitalPrac.Controllers
{
    public class FibonacciController : ApiController
    {

        public int GetFibonacciIterations(int iterations)
        {
            return BluegrassDigitalPrac.Helpers.FibonacciHelper.calculateFibonacciAt(iterations);
        }

        public int GetFibonacciRecursion(int recursions)
        {
            return BluegrassDigitalPrac.Helpers.FibonacciHelper.calculateFibonacciRecursivly(recursions); 
        }

    }
}
