﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BluegrassDigitalPrac.Models;
using Newtonsoft.Json;

namespace BluegrassDigitalPrac.Controllers
{
    public class MenuController : ApiController
    {
        public Menu getMenuById(int PageId)
        {
            return new Menu(PageId);
        }

        public Menu getMenuByPage(string pageName)
        {
            return new Menu(pageName);
        }
    }
}
