﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BluegrassDigitalPrac.Helpers
{
    public static class FibonacciHelper
    {
        public static int calculateFibonacciAt(int n)
        {
            List<int> FibonacciSequence = new List<int> { 0, 1 };
            if (n <= 0)
            {//Numbers that are negative or zero, return 0
                return -1;
            }
            else if (n <= 2)
            {//No loops needed for the first two, they are "hard coded".
                return FibonacciSequence[n - 1];
            }
            else
            {//Here we need to calculate the answer but incrementing the list
                for (int i = 2; i < n; i++)
                {
                    FibonacciSequence.Add(
                        FibonacciSequence[i - 2] +  //Start at pos 0
                        FibonacciSequence[i - 1]    //Start at pos 1
                        );
                }
                return FibonacciSequence.Last();
            }
        }

        public static int calculateFibonacciRecursivly(int target, int pos = 0, int a = 0, int b = 1)
        {
            if (target <= 0)
            {
                return -1;
            }
            else if (target == 1)
            {
                return a;
            }
            else if (target == 2)
            {
                return b;
            }
            else
            {
                if (pos < target - 3)
                {
                    return calculateFibonacciRecursivly(target, pos + 1, b, a + b);
                }
                else
                {
                    return a + b;
                }
            }
        }


    }
}

/*
 * Pos  Answer
 *  1	0
 *  2	1
 *  3	1
 *  4	2
 *  5	3
 *  6	5
 *  7	8
 *  8	13
 *  9	21
 *  10	34
 *  11	55
 *  12	89
 *  13	144
 *  14	233
 *  15	377
 *  16	610
 *  17	987
 *  18	1597
 *  19	2584
 *  20	4181
 *  21	6765
 *  22	10946
 *  23	17711
 *  24	28657
 *  25	46368
 */
