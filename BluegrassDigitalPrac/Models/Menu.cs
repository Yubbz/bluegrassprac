﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace BluegrassDigitalPrac.Models
{

    [Serializable]
    public class Menu
    {
        [JsonProperty]
        public int PageId;
        [JsonProperty]
        private String PageName;
        [JsonProperty]
        private List<Menu> Children = new List<Menu>();

        public Menu(int pageId)
        {
            createMenu(pageId);         
        }

        public Menu(string pageName)
        {
            createMenu(validateMenuName(pageName));
        }

        public void createMenu(int pageId)
        {
            using (SqlConnection conn = new SqlConnection(WebConfigurationManager.ConnectionStrings["PracticalConnection"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    conn.Open();
                    cmd.Connection = conn;
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "SELECT pageId, PageName FROM PageInATable WHERE PageId = @PageName";
                    cmd.Parameters.AddWithValue("@PageName", pageId);
                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        this.PageId = reader.GetInt32(0);
                        this.PageName = reader.GetString(1);
                        this.Children = getChildren(this.PageId);
                    }

                    reader.Close();
                    conn.Close();
                }
            }
        }

        private List<Menu> getChildren(int ParentId)
        {
            List<Menu> children = new List<Menu>(); 
            using (SqlConnection conn = new SqlConnection(WebConfigurationManager.ConnectionStrings["PracticalConnection"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    conn.Open();
                    cmd.Connection = conn;
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "SELECT pageId FROM PageInATable WHERE ParentId = @ParentId";
                    cmd.Parameters.AddWithValue("@ParentId", ParentId);
                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        children.Add(new Menu(reader.GetInt32(0)));
                    }

                    reader.Close();
                    conn.Close();
                }
            }



            return children;
        }

        /// <summary>
        /// Method Takes in the name of a page and returns its Id.
        /// </summary>
        /// <param name="menuName">The Name of the menu</param>
        /// <returns>The integer Id of the menuItem</returns>
        public int validateMenuName(string menuName)
        {
            using (SqlConnection conn = new SqlConnection(WebConfigurationManager.ConnectionStrings["PracticalConnection"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    conn.Open();
                    cmd.Connection = conn;
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "SELECT pageId FROM PageInATable WHERE PageName = @PageName";
                    cmd.Parameters.AddWithValue("@PageName", menuName);
                    SqlDataReader reader = cmd.ExecuteReader();

                    if (reader.Read())
                    {
                        int pageId = reader.GetInt32(0);
                        reader.Close();
                        conn.Close();
                        return pageId;
                    }
                    else
                    {
                        reader.Close();
                        conn.Close();
                        return -1;
                    }
                }
            }
        }
    }

}